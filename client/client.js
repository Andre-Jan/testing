//NEED TO SEND MESSAGES LIST TO MESSAGE TEMPLATE.

Template.messages.messages = function () {
  return MSG.find({}, {sort:{time: -1}});
};

Template.errors.error = function () {
  return ERRORS.find( {} , {sort:{time:-1}});
};

//GENERAL FORM: Template.<template-name>.<template-variable>

//DEFINE INPUT HANDLER
// Test for blank message
Template.new_msg.events = {
  'click input.add': function () {
    // notice the added trim()
    var new_msg = document.getElementById("new_msg").value.trim();
    var name = "JACO G";


    //DO VALIDATION TESTS ON new_msg
    if (new_msg) {
      MSG.insert({
        name: name,
        msg:new_msg,
        time:Date.now()
      });

    } else {
      ERRORS.insert({
        field: "default msg",
        error_msg:"Please input a message",
        time:Date.now()
      });
    }
  }
};

// Only one letter at at time, must be upper case
Template.new_char.events = {
  'click input.add': function () {
    // notice the added trim()
    var new_char = document.getElementById("new_char").value.trim();
    var name = "CHAR GUY";

    // Only check if it is one letter
    if (new_char && new_char.length === 1) {
      // Force uppercase
      new_char = new_char.toUpperCase();
      MSG.insert({
        name: name,
        msg:new_char,
        time:Date.now()
      });
    } else {
      ERRORS.insert({
        field: "char",
        error_msg:"Please input a single letter",
        time:Date.now()
      });


    }
    // here's the valid_name check
  }
};

// Inputing a valid number
Template.new_math.events = {
  'click input.add': function () {
    // notice the added trim()
    var new_int = document.getElementById("new_math").value.trim();
    var name = "INT GUY";

    var n = ~~Number(new_int);

    if (new_int && String(n) == new_int && n >= 0) {  
      MSG.insert({
        name: name,
        msg: new_int,
        time: Date.now()
      });
    }
    else{
      ERRORS.insert({
        field: "int",
        error_msg:"Please input a valid number",
        time:Date.now()
      });
    }
    // here's the valid_name check
  }
};

    
